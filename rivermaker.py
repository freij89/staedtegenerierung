import numpy as np
import random
import util
import math

class RiverMaker:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        flip = random.randint(0,1)
        bit = random.randint(0,1)
        self.flip = flip
        if flip == 0:
            self.goal = (random.randint(0,self.width-1), bit*(self.height-1))
            self.start = (random.randint(0,self.width-1), (1-bit)*(self.height-1))
        elif flip == 1:
            self.goal = ((self.width-1)*bit, random.randint(0,self.height-1))
            self.start = ((self.width-1)*(1-bit), random.randint(0,self.height-1))
        self.grid = np.ones((self.width, self.height))
        for i in range(self.width):
            for j in range(self.height):
                self.grid[i,j] = random.randint(1,80)

    def genRiver(self):
        fringe = util.PriorityQueue()
        fringe.push( (self.start, [], [], 0), util.manhattanDistance(self.start, self.goal))

        expanded = []

        sea = self.getArea(self.goal, 2)

        while not fringe.isEmpty():
            v, path, visited, costs = fringe.pop()

            if not v in expanded:
                expanded.append(v)

                if v == self.goal:
                    starts = []
                    (x,y) = path[math.floor(len(path)/2)]
                    if self.flip == 0:
                        starts.append((x-3, y))
                        starts.append((x+3, y))
                    else:
                        starts.append((x, y-3))
                        starts.append((x, y+3))
                    return (path + [self.goal] + sea, starts)

                for next, newaction in self.getAStarNeighbours(v, -1, 1):
                    cost = self.grid[next[0], next[1]]
                    if not next in visited:
                        fringe.push( (next, path + [v], visited + [v], costs + cost), costs + cost + util.manhattanDistance(self.goal, next))

        return []

    def getArea(self, loc, size):
        area = []
        (x,y) = loc
        for i in range(size+1):
            for j in range(size+1):
                # chance = random.randint(0,size)
                # if chance >= i:
                #     continue
                area.append((util.clamp(0,x-i,self.width-1), util.clamp(0,y-j,self.height-1)))
                area.append((util.clamp(0,x+i,self.width-1), util.clamp(0,y-j,self.height-1)))
                area.append((util.clamp(0,x-i,self.width-1), util.clamp(0,y+j,self.height-1)))
                area.append((util.clamp(0,x+i,self.width-1), util.clamp(0,y+j,self.height-1)))

        area = list(dict.fromkeys(area))
        return area

    def getAStarNeighbours(self, curr, action, nextstep):
        (x,y) = curr
        copy = (curr, action)
        res = []
        if x-nextstep >= 0:
            res.append( ((x-nextstep,y),0) )
            if action == 0:
                copy = ((x-nextstep,y),0)
        if x+nextstep < self.width:
            res.append( ((x+nextstep,y),1) )
            if action == 1:
                copy = ((x+nextstep,y),1)
        if y-nextstep >= 0:
            res.append( ((x,y-nextstep),2) )
            if action == 2:
                copy = ((x,y-nextstep),2)
        if y+nextstep < self.height:
            res.append( ((x,y+nextstep),3) )
            if action == 3:
                copy = ((x,y+nextstep),3)
        return res
