import numpy as np
import random
import util
import math

class BuildingMaker():
    def __init__(self, width, height, rivers, streets):
        self.width = width
        self.height = height
        self.rivers = rivers
        self.streets = streets

    def generateBuildings(self):
        buildings = []
        for street in self.streets:
            buildings = buildings + self.getArea(street, 3)
        self.buildings = list(dict.fromkeys(buildings))
        buildings = [i for i in buildings if i not in self.streets and i not in self.rivers]
        return buildings

    def getArea(self, loc, size):
        area = []
        (x,y) = loc
        for i in range(size+1):
            chance = random.randint(0,size)
            if chance >= i:
                area.append((util.clamp(0,x-i,self.width-1), y))
            chance = random.randint(0,size)
            if chance >= i:
                area.append((util.clamp(0,x+i,self.width-1), y))
            chance = random.randint(0,size)
            if chance >= i:
                area.append((x, util.clamp(0,y-i,self.height-1)))
            chance = random.randint(0,size)
            if chance >= i:
                area.append((x, util.clamp(0,y+i,self.height-1)))

        area = list(dict.fromkeys(area))
        return area
