import numpy as np
import random
import util
import math

class StreetMaker():
    def __init__(self, width, height, rivers, rivercount):
        self.width = width
        self.height = height
        self.rivers = rivers
        self.rivercount = rivercount

    def generateStreets(self, start=(-1,-1)):
        newstart = (-1,-1)
        if start == (-1,-1):
            start = (math.floor(self.width/2),math.floor(self.height/2))
        fringe = util.Stack()
        res = []
        fringe.push( (start, -1, [], 0) )
        expanded = []
        count = 0;
        while not fringe.isEmpty() and count < self.height*self.width/3/7/(self.rivercount+1):
        # while not fringe.isEmpty():
            count += 1
            fringe.shuffle()
            v, action, visited, step = fringe.pop()
            nextstep = random.randint(2,4)*3
            # nextstep = 1
            if not v in expanded:
                expanded.append(v)
                area = []
                res.append(v)
                for i in range(step+1):
                    (x,y) = v
                    if action == 0:
                        x = v[0]+i
                        area.append((x, y+1))
                        area.append((x, y-1))
                        # area.append((x, y+2))
                        # area.append((x, y-2))
                    elif action == 1:
                        x = v[0]-i
                        area.append((x, y+1))
                        area.append((x, y-1))
                        # area.append((x, y+2))
                        # area.append((x, y-2))
                    elif action == 2:
                        y = v[1]+i
                        area.append((x+1, y))
                        area.append((x-1, y))
                        # area.append((x+2, y))
                        # area.append((x-2, y))
                    elif action == 3:
                        y = v[1]-i
                        area.append((x+1, y))
                        area.append((x-1, y))
                        # area.append((x+2, y))
                        # area.append((x-2, y))
                    expanded.append((x,y))
                    area.append((x,y))
                    expanded = expanded + area
                    res.append((x,y))

                for next, newaction in self.getNeighbours(v, action, nextstep):
                    if self.crossesRiver(v, next):
                        newstart = next
                        continue
                    if (not next in visited) and not self.isReverseAction(action, newaction):
                        fringe.push( (next, newaction, visited + [v] + area, nextstep) )
            else:
                chance = 0.30
                if random.random() < chance:
                    for i in range(step):
                        if action == 0 and v[0]+1+i < self.width:
                            # zvals[v[0]+1+i, v[1]] = 0
                            res.append((v[0]+1+i, v[1]))
                        elif action == 1 and v[0]-1-i >= 0:
                            # zvals[v[0]-1-i, v[1]] = 0
                            res.append((v[0]-1-i, v[1]))
                        elif action == 2 and v[1]+1+i < self.height:
                            # zvals[v[0], v[1]+1+i] = 0
                            res.append((v[0], v[1]+1+i))
                        elif action == 3 and v[1]-1-i >= 0:
                            # zvals[v[0], v[1]-1-i] = 0
                            res.append((v[0], v[1]-1-i))
        res = list(dict.fromkeys(res))
        out = []
        top = list(res)
        bot = list(res)
        left = list(res)
        right = list(res)
        lout = []
        rout = []
        tout = []
        bout = []
        top.sort(key=lambda street: street[0])
        bot.sort(key=lambda street: street[0], reverse=True)
        left.sort(key=lambda street: street[1])
        right.sort(key=lambda street: street[1], reverse=True)
        toConnect = random.sample(range(12), 2)
        for i in toConnect:
            x,y = top[i]
            a,b = bot[i]
            while x > 0:
                x -= 1
                lout.append((x,y))
            x,y = top[i]
            a,b = bot[i]
            while a < self.width-1:
                a += 1
                rout.append((a,b))
            x,y = left[i]
            a,b = right[i]
            while y > 0:
                y -= 1
                tout.append((x,y))
            x,y = left[i]
            a,b = right[i]
            while b < self.height-1:
                b += 1
                bout.append((a,b))

        out.append(lout)
        out.append(rout)
        out.append(tout)
        out.append(bout)

        return res, out

    def crossesRiver(self, start, end):
        # return False
        for i in range(util.manhattanDistance(start, end)):
            if (start[0] < end[0]):
                if (start[0]+i+1, start[1]) in self.rivers:
                    return True
            elif (start[0] > end[0]):
                if (start[0]-i-1, start[1]) in self.rivers:
                    return True
            if (start[1] < end[1]):
                if (start[0], start[1]+i+1) in self.rivers:
                    return True
            elif (start[1] > end[1]):
                if (start[0], start[1]-i-1) in self.rivers:
                    return True

        return False

    def isReverseAction(self, action1, action2):
        if action1 == 1 and action2 == 0 or action1 == 0 and action2 == 1:
            return True
        if action1 == 2 and action2 == 3 or action1 == 3 and action2 == 2:
            return True
        return False

    def getStrip(self, start, end, width):
        strip = []
        lu = (0,0)
        rl = (0,0)
        if start[0] != end[0]:
            lu = (util.clamp(0,min(start[0], end[0]), self.width-1), util.clamp(0,start[1]-width, self.height-1))
            rl = (util.clamp(0,max(start[0], end[0]), self.width-1), util.clamp(0,start[1]+width, self.height-1))
        else:
            lu = (util.clamp(0,start[0]-width, self.width-1), util.clamp(0,min(start[1], end[1]), self.height-1))
            rl = (util.clamp(0,start[0]+width, self.width-1), util.clamp(0,max(start[1], end[1]), self.height-1))
        for i in range(rl[0]-lu[0]+1):
            for j in range(rl[1]-lu[1]+1):
                strip.append((lu[0]+i, lu[1]+j))
        return strip

    def getNeighbours(self, curr, action, nextstep):
        (x,y) = curr
        copy = (curr, action)
        res = []
        if x-nextstep >= 0:
            res.append( ((x-nextstep,y),0) )
            if action == 0:
                copy = ((x-nextstep,y),0)
        if x+nextstep < self.width:
            res.append( ((x+nextstep,y),1) )
            if action == 1:
                copy = ((x+nextstep,y),1)
        if y-nextstep >= 0:
            res.append( ((x,y-nextstep),2) )
            if action == 2:
                copy = ((x,y-nextstep),2)
        if y+nextstep < self.height:
            res.append( ((x,y+nextstep),3) )
            if action == 3:
                copy = ((x,y+nextstep),3)
        # random.shuffle(res)
        for i in range(2):
            res.append(copy)
        return res
