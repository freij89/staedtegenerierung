import matplotlib as mpl
from matplotlib import pyplot
import numpy as np
import random
import util
import math
from rivermaker import RiverMaker
from streetmaker import StreetMaker
from buildingmaker import BuildingMaker

class City():
    def __init__(self, width=200, height=200, size=5):
        self.zvals = np.ones((width,height))
        self.rivers = []
        self.streets = []
        self.buildings = []
        self.width = width
        self.height = height
        self.out = []
        self.starts = []
        self.citystreets = []

    def genRivers(self):
        rm = RiverMaker(self.width, self.height)

        rivers, starts = rm.genRiver()
        self.starts = starts
        self.rivers = self.rivers + rivers

        for point in self.rivers:
            self.zvals[point[0], point[1]] = -2

    def genStreets(self):
        # streetmaker = StreetMaker(self.width, self.height, self.rivers, self.rivercount)
        self.citystreets = []
        for start in self.starts:
            streetmaker = StreetMaker(self.width, self.height, self.rivers, self.rivercount)

            streets, out = streetmaker.generateStreets(start=start)
            self.citystreets.append(streets)
            self.out = self.out + out
            self.streets = self.streets + streets

        if self.starts == []:
            streetmaker = StreetMaker(self.width, self.height, self.rivers, self.rivercount)

            streets, out = streetmaker.generateStreets()
            self.citystreets.append(streets)
            self.out = self.out + out
            self.streets = self.streets + streets
        else:
            start = self.starts[0]
            end = self.starts[1]
            dist = util.manhattanDistance(start, end)
            if start[0] == end[0]:
                if start[1] < end[1]:
                    for i in range(dist):
                        self.streets.append((start[0], start[1]+i))
                        self.zvals[start[0], start[1]+i] = 0
                else:
                    for i in range(dist):
                        self.streets.append((start[0], start[1]-i))
                        self.zvals[start[0], start[1]-i] = 0
            else:
                if start[0] < end[0]:
                    for i in range(dist):
                        self.streets.append((start[0]+i, start[1]))
                        self.zvals[start[0]+i, start[1]] = 0
                else:
                    for i in range(dist):
                        self.streets.append((start[0]-i, start[1]))
                        self.zvals[start[0]-i, start[1]] = 0


        for street in self.streets:
            self.zvals[street] = 0



    def genBuildings(self):
        bm = BuildingMaker(self.width, self.height, self.rivers, self.streets)

        self.buildings = self.buildings + bm.generateBuildings()

        for building in self.buildings:
            self.zvals[building] = -1

    def outStreets(self):
        x = 0
        print(len(self.citystreets))
        for street in self.out:
            for pixel in street:
                if len(self.citystreets) > 1:
                    citynum = 1-math.floor(x/4)
                    if pixel in self.citystreets[citynum]:
                        break
                self.zvals[pixel] = 0
            x += 1

    def rivercount(self, count):
        self.rivercount = count

try:
    width = int(input('Enter Height '))
    height = int(input('Enter Width '))
    rivers = int(input('Enter number of rivers '))
except ValueError:
    print('No correct value given')
    exit()
city = City(width, height)
# city.genStreets()
city.rivercount(rivers)
for i in range(rivers):
    city.genRivers()

city.genStreets()
city.genBuildings()
city.outStreets()

# make a color map of fixed colors
cmap = mpl.colors.ListedColormap(['yellow','blue','red','black','green'])
bounds=[-3.5, -2.5,-1.5,-0.5,0.5,1]
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

# tell imshow about color map so that only set colors are used
img = pyplot.imshow(city.zvals,interpolation='nearest',
                    cmap = cmap,norm=norm)

pyplot.show()
